# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.54.0] - 2023-06-22
### Added
- Initial import

[Unreleased]: https://gitlab.com/zedtux/rubocop-in-docker/compare/v0.54.0...master
[0.54.0]: https://gitlab.com/zedtux/rubocop-in-docker/-/tags/v0.54.0
