# Rubocop in Docker

This project uses the Docker image from [pipeline-components](https://gitlab.com/pipeline-components/rubocop)
installs Rubocop extensions (rails and rspec as of writing), and uses a shell
script to convert absolut path to relative path, see more bellow.

The goal of this project is to allow using Rubocop from a Docker container in
Sublime Text using the Sublime Text plugin [Sublime RuboCop](https://github.com/pderichs/sublime_rubocop).

## Installation

1. Install Docker, Sublime and the [Sublime RuboCop](https://github.com/pderichs/sublime_rubocop) plugin
2. Create an executable bash script file, in your PATH with the following:
```bash
#!/usr/bin/env bash

docker run --rm -v ${PWD}:/code registry.gitlab.com/zedtux/rubocop-in-docker:latest "${PWD}/" "$@"
```

## Why converting path

Your project folder is mounted in the Docker container as the `/code` folder,
the `WORKDIR` of the Docker image, so that the path in the container would be
`/code/project/app/models/user.rb` for example.

Sublime RuboCop plugin passes the absolute path to the local `rubocop` command
which makes `rubocop` running in the container not able to find it since it is
actually mounted in the `/code` folder.

This project installs the `entrypoint.sh` script in the container which take
care of converting absolute path to relative path.
