FROM registry.gitlab.com/pipeline-components/rubocop:0.54.0

# Installs Rubocop extensions until https://gitlab.com/pipeline-components/rubocop/-/issues/2
# is closed.
RUN gem install rubocop-rails \
                rubocop-rspec

COPY entrypoint.sh /entrypoint

ENTRYPOINT ["/entrypoint"]
