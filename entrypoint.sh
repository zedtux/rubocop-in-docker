#!/usr/bin/env sh

#
# The aim of this script is to convert absolute path, given by Sublime Text,
# to relative path since the dev's project is mounted in the `/code` folder.
#
# When first argument is /home/me/devs/my_project/ and the next arguments,
# containing the path to the file that Rubocop should analyse,
# is /home/me/devs/my_project/app/folder/file.rb, then this script will run
# `rubocop app/folder/file.rb`.
#

WORKDIR=$1

# Removes the first argment which has been sorted in `WORKDIR`
shift

rubocop $(echo $@ | sed -e 's#'$WORKDIR'##')
